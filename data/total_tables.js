//Ejemplo obtener un array de un array de objetos con key servicio
//let result = data.map(a => a.servicio);
//Ejemplo para seleccionar una columna de una matriz
//const arrayColumn = (array, column) => array.map(e => e[column]);
//console.log(arrayColumn(outputData, 2));

//servicios
//Servicio Local de Telecomunicaciones Primer Contrato
//Servicio Local de Telecomunicaciones Contrato San Borja
//Servicio Local de Telecomunicaciones Adenda
//Servicio de Alquiler de Circuitos Contrato SLD
//Servicio de Alquiler de Circuitos Contrato San Borja SL
//Servicio de Alquiler de Circuitos Primer Contrato SL
//Servicio de Comunicaciones Personales
//Servicio de Larga Distancia Nacional e Internacional	
//Servicio de Portadores
//Servicio de Telefonos Publicos Contrato San Borja	
//Servicio de Telefonos Publicos Primer Contrato

//convierte un array de objetos en un array de arrays
var outputData = data.map( Object.values); 

//para seleccionar elementos desde la columna 0 hasta la columna 27
var datos_tabla = outputData.map(row => row.map(element => element).slice(0,28));

$(document).ready(function() {

    var table = $('#tabla_total').DataTable( {
        data:datos_tabla,
        columns: [
            { title: "Servicio" },
            { title: "Area" },
            { title: "Nro" },
            { title: "Tipo Meta" },
            { title: "Meta" },
            { title: "V. objetivo" },
            { title: "V. reportado [S1]" },
            { title: "V. reportado [S2]" },
            { title: "V. reportado [Anual]" },
            { title: "V. verificado [S1]" },
            { title: "V. verificado [S2]" },
            { title: "V. verificado [Anual]" },
            { title: "V. referencial [S1]" },
            { title: "V. referencial [S2]" },
            { title: "V. referencial [Anual]" },
            { title: "Reportado vs Verificado [S1]" },
            { title: "Reportado vs Verificado [S2]" },
            { title: "Reportado vs Verificado [Anual]" },
            { title: "Objetivo vs Verificado" },
            { title: "Dictamen" },
            { title: "Dictamen referencial" },
            { title: "Objetivo" },
            { title: "Conclusión 1" },
            { title: "Conclusión 2" },
            { title: "Conclusión 3" },
            { title: "Recomendación 1" },
            { title: "Recomendación 2" },
            { title: "Recomendación 3" },            
        ],
        bFilter: true,
        paging: true,       //mostrar la paginacion
        lengthMenu: [[12, 24, 36, -1], [12, 24, 36, "All"]], //intervalos de la paginacion
        bInfo: true,        //muestra la informacion acerca del numero de resultados
        order: [[ 0, "asc" ],[ 2, "asc" ]], //ordena la columna y 3ra columna de manera descendente desc p asc ascendente
        scrollY: 400,        //Scroll vertical
        scrollX: true,       //habilitando scroll horizontal
        bAutoWidth: true, // Disable the auto width calculation
        bScrollCollapse: true,
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            },
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                title: 'Metas de Calidad Entel 2016 - Servicio Local de Telecomunicaciones',
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [':visible',25,26,27,22,23,24]
                }
            },
            {
                title: 'Metas de Calidad Entel 2016 - Servicio Local de Telecomunicaciones',
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'SRA2',
                exportOptions: {
                    columns: [ 0,1,3,4,5,6,7,8,9,10,11,12,13,14,19]
                }
            }        
        ],
        columnDefs: [
            {
                targets: [2,21,22,23,24,25,26,27],
                className: 'noVis'
            },
            {
                "render": function ( data, type, row ) {
                    
                    return data + '\n'+ row[23]+'\n'+row[24];
                },
                "targets": 22
            },
            {
                "render": function ( data, type, row ) {
                    
                    return data + '\n'+ row[26]+'\n'+row[27];
                },
                "targets": 25
            },
            { "visible": false,  "targets": [2,21,22,23,24,25,26,27] },
            {
            "targets": [6,7,8,9,10,11,12,13,14,21],
            "render": function ( data, type, full, meta ) {            
                if (isNaN(data)) 
                        return data;
                if (data === "") 
                        return data; 
                    else
                        nuevovalor = data*100;	            	
                    nuevovalor2 =nuevovalor.toFixed(2); 		
                    return +nuevovalor2+'%';				
                }
            }
        ],
        language: {
            "decimal": ",",
            "thousands": ".",       //para poner , en los decimales y . en miles
            "lengthMenu": "Mostrando _MENU_ registros por página",       //y otras leyendas (show entries)
            "zeroRecords": "Ningun registro encontrado",
            "info": "Página _PAGE_ de _PAGES_ páginas",
            "infoEmpty": "No existe registros",
            "infoFiltered": "(filtrado de _MAX_ total de registros)",
            "search":"Buscar",
            "paginate":{
                "previous": "Anterior",
                "next": "Siguiente"},
            "buttons": {
                "copy": 'Copiar',
                "print": 'Imprimir',
                "colvis": 'Visibilidad de columnas',
                "copySuccess": {
                    "1": "Copiado al portapapeles",
                    "_": "Copiando %d filas al portapapeles"
                },
                "copyTitle": 'Copiar al portapapeles'
            }                       
        },
        //responsive: true, 
        deferRender: true,
        initComplete: function () {
            this.api().columns([1]).every( function () {
              var column = this;
              var select = $("#areaFltr"); 
              column.data().unique().sort().each( function ( d, j ) {
                select.append( '<option value="'+d+'">'+d+'</option>' )
              } );
            } );

             this.api().columns([3]).every( function () {
              var column = this;
              var select = $("#tipoFltr"); 
              column.data().unique().sort().each( function ( d, j ) {
                select.append( '<option value="'+d+'">'+d+'</option>' )
              } );
            } );
            this.api().columns([4]).every( function () {
                var column = this;
                var select = $("#metaFltr"); 
                column.data().unique().sort().each( function ( d, j ) {
                  select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
              } );
              this.api().columns([19]).every( function () {
                var column = this;
                var select = $("#dictamenFltr"); 
                column.data().unique().sort().each( function ( d, j ) {
                  select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
              } );
              this.api().columns([0]).every( function () {
                var column = this;
                var select = $("#serFltr"); 
                column.data().unique().sort().each( function ( d, j ) {
                  select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
              } );
            $("#areaFltr,#tipoFltr,#metaFltr,#dictamenFltr,#serFltr").select();
         }
      });

    $('#serFltr').on('change', function(){
        var search = [];
    
        $.each($('#serFltr option:selected'), function(){
                search.push($(this).val());
        });
        
        search = search.join('|');
        table.column(0).search(search ? '^'+search+'$' : '', true, false).draw();

        //desabilitando el dropdown
        $("#serFltr").prop("disabled", true);

        
        if($( "#areaFltr option:selected" ).text()==''){
            var selectArea = $("#areaFltr");
            selectArea.empty();
            selectArea.append( '<option value=""></option>' );
            table.column(1, { search:'applied' } ).data().unique().each(function(value, index) {
                selectArea.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }


        if($( "#tipoFltr option:selected" ).text()==''){
            var selectTipo = $("#tipoFltr");
            selectTipo.empty();
            selectTipo.append( '<option value=""></option>' );
            table.column(3, { search:'applied' } ).data().unique().each(function(value, index) {
                selectTipo.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }


        if($( "#metaFltr option:selected" ).text()==''){
            var selectMeta = $("#metaFltr");
            selectMeta.empty();
            selectMeta.append( '<option value=""></option>' );
            table.column(4, { search:'applied' } ).data().unique().each(function(value, index) {
                selectMeta.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }        
        
        if($( "#dictamenFltr option:selected" ).text()==''){
            var selectDic = $("#dictamenFltr");
            selectDic.empty();
            selectDic.append( '<option value=""></option>' );
            table.column(19, { search:'applied' } ).data().unique().each(function(value, index) {
                selectDic.append( '<option value="'+value+'">'+value+'</option>' );
            });
        } 
    });

    $('#areaFltr').on('change', function(){
        var search = [];
    
        $.each($('#areaFltr option:selected'), function(){
                search.push($(this).val());
        });
        
        search = search.join('|');
        table.column(1).search(search ? '^'+search+'$' : '', true, false).draw();

        //desabilitando el dropdown
        $("#areaFltr").prop("disabled", true);
        
        if($( "#serFltr option:selected" ).text()==''){
            var selectSer = $("#serFltr");
            selectSer.empty();
            selectSer.append( '<option value=""></option>' );
            table.column(0, { search:'applied' } ).data().unique().each(function(value, index) {
                selectSer.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }


        if($( "#tipoFltr option:selected" ).text()==''){
            var selectTipo = $("#tipoFltr");
            selectTipo.empty();
            selectTipo.append( '<option value=""></option>' );
            table.column(3, { search:'applied' } ).data().unique().each(function(value, index) {
                selectTipo.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }


        if($( "#metaFltr option:selected" ).text()==''){
            var selectMeta = $("#metaFltr");
            selectMeta.empty();
            selectMeta.append( '<option value=""></option>' );
            table.column(4, { search:'applied' } ).data().unique().each(function(value, index) {
                selectMeta.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }
        
        
        if($( "#dictamenFltr option:selected" ).text()==''){
            var selectDic = $("#dictamenFltr");
            selectDic.empty();
            selectDic.append( '<option value=""></option>' );
            table.column(19, { search:'applied' } ).data().unique().each(function(value, index) {
                selectDic.append( '<option value="'+value+'">'+value+'</option>' );
            });
        } 
    });
    
    $('#tipoFltr').on('change', function(){
        var search = [];
    
        $.each($('#tipoFltr option:selected'), function(){
                search.push($(this).val());
        });
        
        search = search.join('|');
        table.column(3).search(search ? '^'+search+'$' : '', true, false).draw();
        
        //desabilitando el dropdown
        $("#tipoFltr").prop("disabled", true);

        if($( "#serFltr option:selected" ).text()==''){
            var selectSer = $("#serFltr");
            selectSer.empty();
            selectSer.append( '<option value=""></option>' );
            table.column(0, { search:'applied' } ).data().unique().each(function(value, index) {
                selectSer.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }


        if($( "#areaFltr option:selected" ).text()==''){
            var selectArea = $("#areaFltr");
            selectArea.empty();
            selectArea.append( '<option value=""></option>' );
            table.column(1, { search:'applied' } ).data().unique().each(function(value, index) {
                selectArea.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }


        if($( "#metaFltr option:selected" ).text()==''){
            var selectMeta = $("#metaFltr");
            selectMeta.empty();
            selectMeta.append( '<option value=""></option>' );
            table.column(4, { search:'applied' } ).data().unique().each(function(value, index) {
                selectMeta.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }
        
        
        if($( "#dictamenFltr option:selected" ).text()==''){
            var selectDic = $("#dictamenFltr");
            selectDic.empty();
            selectDic.append( '<option value=""></option>' );
            table.column(19, { search:'applied' } ).data().unique().each(function(value, index) {
                selectDic.append( '<option value="'+value+'">'+value+'</option>' );
            });
        } 
    
    });

 
    $('#metaFltr').on('change', function(){
        var search = [];
        
        $.each($('#metaFltr option:selected'), function(){
                search.push($(this).val());
        });
        
        search = search.join('|');
        table.column(4).search(search ? '^'+search+'$' : '', true, false).draw();


        //desabilitando el dropdown
        $("#metaFltr").prop("disabled", true);

        if($( "#serFltr option:selected" ).text()==''){
            var selectSer = $("#serFltr");
            selectSer.empty();
            selectSer.append( '<option value=""></option>' );
            table.column(0, { search:'applied' } ).data().unique().each(function(value, index) {
                selectSer.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }


        if($( "#areaFltr option:selected" ).text()==''){
            var selectArea = $("#areaFltr");
            selectArea.empty();
            selectArea.append( '<option value=""></option>' );
            table.column(1, { search:'applied' } ).data().unique().each(function(value, index) {
                selectArea.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }


        if($( "#tipoFltr option:selected" ).text()==''){
            var selectTipo = $("#tipoFltr");
            selectTipo.empty();
            selectTipo.append( '<option value=""></option>' );
            table.column(3, { search:'applied' } ).data().unique().each(function(value, index) {
                selectTipo.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }
        
        
        if($( "#dictamenFltr option:selected" ).text()==''){
            var selectDic = $("#dictamenFltr");
            selectDic.empty();
            selectDic.append( '<option value=""></option>' );
            table.column(19, { search:'applied' } ).data().unique().each(function(value, index) {
                selectDic.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }        
        
    });


    $('#dictamenFltr').on('change', function(){
        var search = [];
    
        $.each($('#dictamenFltr option:selected'), function(){
                search.push($(this).val());
        });
        
        search = search.join('|');
        table.column(19).search(search ? '^'+search+'$' : '', true, false).draw();

        //desabilitando el dropdown
        $("#dictamenFltr").prop("disabled", true);

        if($( "#serFltr option:selected" ).text()==''){
            var selectSer = $("#serFltr");
            selectSer.empty();
            selectSer.append( '<option value=""></option>' );
            table.column(0, { search:'applied' } ).data().unique().each(function(value, index) {
                selectSer.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }


        if($( "#areaFltr option:selected" ).text()==''){
            var selectArea = $("#areaFltr");
            selectArea.empty();
            selectArea.append( '<option value=""></option>' );
            table.column(1, { search:'applied' } ).data().unique().each(function(value, index) {
                selectArea.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }


        if($( "#tipoFltr option:selected" ).text()==''){
            var selectTipo = $("#tipoFltr");
            selectTipo.empty();
            selectTipo.append( '<option value=""></option>' );
            table.column(3, { search:'applied' } ).data().unique().each(function(value, index) {
                selectTipo.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }
        
        
        if($( "#metaFltr option:selected" ).text()==''){
            var selectMeta = $("#metaFltr");
            selectMeta.empty();
            selectMeta.append( '<option value=""></option>' );
            table.column(4, { search:'applied' } ).data().unique().each(function(value, index) {
                selectMeta.append( '<option value="'+value+'">'+value+'</option>' );
            });
        }

            
    });
    
});


